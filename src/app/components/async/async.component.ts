import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-async',
  templateUrl: './async.component.html',
  styleUrls: ['./async.component.scss']
})
export class AsyncComponent implements OnInit {
  nos: String;

  sorted: String;

  constructor(private http: HttpService) {
    this.nos = '13, 12,-3, 48,   -6';
    this.sorted = '';
  }

  ngOnInit() {
  }

  async testServer() {
    this.sorted = '';

    let nos = this.nos.replace(/\s/g, '').split(',').filter(r => r);
    let r: any = await this.http.sort(nos).toPromise();

    this.sorted = r.error ? r.error.message : r.data.join(', ');
  }

}
