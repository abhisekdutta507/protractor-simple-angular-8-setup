import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../modules/shared/shared.module';
import { AsyncRoutingModule } from './async-routing.module';
import { AsyncComponent } from './async.component';


@NgModule({
  declarations: [AsyncComponent],
  imports: [
    SharedModule,
    CommonModule,
    AsyncRoutingModule
  ]
})
export class AsyncModule { }
