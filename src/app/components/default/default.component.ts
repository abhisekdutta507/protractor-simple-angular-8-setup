import { Component, OnInit } from '@angular/core';
import { Input } from '../../interfaces/input';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  public nos: Array<Input>;

  public greatest: Number;

  constructor() {
    this.nos = [
      { id: 0, value: '' },
      { id: 1, value: '' },
      { id: 2, value: '' },
    ];

    this.greatest = 0;
  }

  ngOnInit() {
  }

  findGreatest() {
    let n1 = Object.assign([], this.nos).map(t => Number(t.value));
    this.greatest = n1.sort((a, b) => a - b).reverse()[0];
  }

}
