import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DefaultRoutingModule } from './default-routing.module';
import { DefaultComponent } from './default.component';

import { SharedModule } from '../../modules/shared/shared.module';


@NgModule({
  declarations: [DefaultComponent],
  imports: [
    CommonModule,
    SharedModule,
    DefaultRoutingModule
  ]
})
export class DefaultModule { }
