import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'async',
    loadChildren: () => import('./components/async/async.module').then(mod => mod.AsyncModule),
  },
  {
    path: 'description',
    loadChildren: () => import('./components/description/description.module').then(mod => mod.DescriptionModule),
  },
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./components/default/default.module').then(mod => mod.DefaultModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
