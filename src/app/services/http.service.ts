import { HttpClient, HttpResponse } from '@angular/common/http'
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private apiurl = 'https://nodeapis101.herokuapp.com/api/v1';

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      Object.assign(result, {'error': error.error})
      return of(result as T);
    };
  }

  testServer(): Observable<HttpResponse<any>> {
    let url = `${this.apiurl}/test`;

    return this.http.get<any>(url);
  }

  sort(array: Array<String>): Observable<HttpResponse<any>> {
    let url = `${this.apiurl}/sort`;

    return this.http.post<any>(url, {array: array}).pipe(
      tap(message => message),
      catchError(this.handleError('sort', {}))
    );;
  }
}
