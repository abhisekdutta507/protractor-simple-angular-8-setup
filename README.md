# Protractor Testing On Angular 8

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.12.

Go to https://musing-khorana-85376c.netlify.com/ to check the live web app.

## Available Scripts

Open the terminal and goto the project root

### Install the dependencies via npm

```bash
npm install
```

### Launch in `development mode`

```bash
npm start
```

The application is launched on `http://localhost:4004/`.

### Start the Selenium server

Run the below code from terminal:

```bash
sudo webdriver-manager start
```

### Test the synchronous and asynchronous modules

Go to `./e2e/` folder in your project root. Make sure the application is running on `http://localhost:4004/`.

Run the test here

```bash
protractor protractor.conf.js
```

The protractor codes can be found in `./e2e/src/app.e2e-spec.ts`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4004/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
