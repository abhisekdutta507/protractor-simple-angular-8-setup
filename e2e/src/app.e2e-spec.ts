import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('Synchronous module', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  /**
   * @description Protractor testing codes for Sync component
   */
  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Find Greatest');
  });

  it('should find the greatest number', async function() {
    let n0: any = page.ebcss('.n0');
    let n1: any = page.ebcss('.n1');
    let n2: any = page.ebcss('.n2');

    let findBtn: any = page.ebid('find-btn');

    let resultText: any = page.ebid('result-text');

    n0.sendKeys(-11);
    n1.sendKeys(1);
    n2.sendKeys(2);

    findBtn.click();

    expect(resultText.getText()).toEqual('2');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

describe('Asynchronous module', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  /**
   * @description Protractor testing codes for Async component
   */
  it('should display welcome message', () => {
    page.navigateTo('http://localhost:4004/async');
    expect(page.getTitleText()).toEqual('Async Sort');
  });

  it('should sort the given integer numbers', async function() {
    let text: any = page.ebcss('.n0');

    let findBtn: any = page.ebid('find-btn');

    let resultText: any = page.ebid('result-text');

    text.clear();
    text.sendKeys('9, 12,-3, 48, -6');

    findBtn.click();

    expect(resultText.getText()).toEqual('-6, -3, 9, 12, 48');
  });

  it('should find the error in given numbers', async function() {
    let text: any = page.ebcss('.n0');

    let findBtn: any = page.ebid('find-btn');

    let resultText: any = page.ebid('result-text');

    text.clear();
    text.sendKeys('9, A12,-3B, 48H, -6');

    findBtn.click();

    expect(resultText.getText()).toEqual('not acceptable');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
