import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(url?: string) {
    return browser.get(url || browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root .content h2')).getText() as Promise<string>;
  }

  ebid(selector: string) {
    return element(by.id(selector)).getText() as Promise<string>;
  }

  ebcss(selector: string) {
    return element(by.css(selector)).getText() as Promise<string>;
  }
}
